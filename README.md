# Fbevents

***Translation below***

Fbevents es una herramienta sencilla para hacer webscrapping (con ayuda de Puppeteer) de cualquier página pública de Facebook y tomar todas las capturas de pantalla posibles o una cantidad a especificar (opcional)

# Limitaciones

* Esta herramienta no distingue entre eventos que ya pasaron y eventos próximos. Si estás recibiendo varios eventos que ya pasaron por favor especifica el número de capturas a tomar cuando uses este comando.

* Desafortunadamente cuando una página tiene un evento recurrente esto genera más de una URL válida distinta para el mismo evento provocando capturas repetidas.

Para poder solucionar los problemas ya mencionados se necesitaría hacer una inspección más profunda de la página y salvo que vea un gran interés en esta herramienta no tengo planes de hacerlo.

Este fue más que nada una prueba de concepto dado que mucha gente me ha mencionado que si están en FB es por los eventos. Bueno, ahora tienen un método (aunque no el mejor) para enterarse de eventos públicos de Facebook sin tener cuenta en ese servicio infernal.

# Uso

```bash
node /path/to/this/project/index.js FACEBOOK_PAGE_NAME
node /path/to/this/project/index.js -f FILE
```

Si se quiere usar un archivo para tomar capturas de varias páginas a la vez se debe seguir este formato

```bash
FACEBOOK_PAGE_NAME,NUMBER_OF_SCREENSHOOTS
```

Recordar que el NUMBER_OF_SCREENSHOOTS es opcional.

# Preguntas recurrentes

## P: ¿Qué es Puppeteer?

[Puppeteer](https://github.com/GoogleChrome/puppeteer) es una librería de Node que provee una API de alto nivel para controlar Chrome o Chromium sobre el [protocolo DevTools](https://chromedevtools.github.io/devtools-protocol/). Puppeteer corre [headless](https://developers.google.com/web/updates/2017/04/headless-chrome) por defecto, pero puede ser configurado para corre Chrome o Chromium completo (no headless).

## P: ¿Cómo funciona el modo headless?

Modo headless = modo normal - interfaz gráfica

## P: ¿Qué hago si aparece un error al iniciar Chromium?

En el repositorio de Puppeteer hay una [apartado especial](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md) para problemas como este. Durante el desarrollo de esta herramiente la solución usada fue [esta](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#alternative-setup-setuid-sandbox).

# TODO

* Cambiar el código para que funcionne correctamente de forma asíncrona
* Separar el código en módulos

# Licencia

Dominio Público

---

# Fbevents

Fbevents is a simple webscrapping tool (with help of Puppeteer) for any public Facebook Page and take all the possible screenshots or a fixed amount (optional).

# Limitations

* This tool doesn't distinguish between past events and upcoming events. If you are getting a lot of past events please specify  the number of screenshots to take when you run it.

* Unfortunely when a page has recurrent events this generates more tha one different valid URL for the same event causing repeated screenshots. 

In order to solve those problems a deeper inspection should be required and unless I see a huge interest for this tool I'm not going to do it. 

This was mostly a proof of concept since a lot of people had told me that they stick to FB only for the events. Well, now you have a method (though not the best) to get (public) events without an account in that hellish "service".

# Usage

```bash
node /path/to/this/project/index.js FACEBOOK_PAGE_NAME
node /path/to/this/project/index.js -f FILE
```

If you want to sue (for example, to use it with more than one FB page)) you need to follow this format:

```bash
FACEBOOK_PAGE_NAME,NUMBER_OF_SCREENSHOOTS
```

Remember that NUMBER_OF_SCREENSHOOTS is optional.

# FAQ

## Q: What is Puppeteer?

[Puppeteer](https://github.com/GoogleChrome/puppeteer) is a Node library which provides a high-level API to control Chrome or Chromium over the [DevTools Protocol](https://chromedevtools.github.io/devtools-protocol/). Puppeteer runs [headless](https://developers.google.com/web/updates/2017/04/headless-chrome) by default, but can be configured to run full (non-headless) Chrome or Chromium.

## Q: What is headless mode?

Headless Mode = Normal Mode - Graphic Interface

## Q: What can I do if I'm getting an error on Chromium's start?

On Puppeteer's repo theres is a [special section](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md) for problems solving. During the development of this tool the solution that worked was [this](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#alternative-setup-setuid-sandbox).

# TODO

* Change the code to properly work asynchronous

* Separate the code on modules

# License

Public Domain
