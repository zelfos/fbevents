"use strict";

const puppeteer = require('puppeteer');
const fs = require('fs');
const readline = require('readline');
const url_head= "https://www.facebook.com/pg/";
const url_tail = "/events/?ref=page_internal";
const fbregex = new RegExp("^.*facebook\.com\/events\/[0-9]*\/(?!dates).*$");
const timeout = 120000;
var _links = "";

function usage () {
	console.log("USAGE: fbevents FACEBOOK_PAGE_NAME");
	console.log("USAGE: fbevents -f FILE");
	process.exit(1)
}

// get all HTML elements of a certain tag
async function getElements (elm,website,regex = null) {
	console.log("Searching events");
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	// Handling promise rejection
	process.on("unhandledRejection", (reason, p) => {
		  console.error("Unhandled Rejection at: Promise", p,
		  "reason:", reason);
		    browser.close();
	});
	await page.goto(website,{timeout: timeout});
	// get all links of a website
	const links = await page.$$eval(elm, 
	anchors => { return anchors.map(anchor => 
	anchor.href) });
	// filter only links that match a regex, in this case only
	// links that looks like FB events
	if (regex !== null ) {
		_links = links.filter(
			RegExp.prototype.test.bind(regex)
		);
	}
	// returning a golbal var because I'm a noob and I don't know
	// how to properly return something from an async function
	else _links = links;
	await browser.close();
};

async function screenshot (website,shots,dir,wd=1080,ht=1200) {
	await getElements('a',website,fbregex);
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	// Handling promise rejection
	process.on("unhandledRejection", (reason, p) => {
		  console.error("Unhandled Rejection at: Promise", p,
		  "reason:", reason);
		    browser.close();
	});
	if ( _links.length === 0 ) {
		console.log("No events found");
	}
	else {
		if ( shots === undefined || shots > _links.length ) { 
			shots = _links.length; 
		}
		console.log(_links.length+" events found");
		console.log("No. of screenshots to take: "+shots);
		for ( let counter=0;counter<shots;counter++ ) {
			console.log("Taking screenshot "+counter);
			await page.goto(_links[counter],
			{timeout: timeout});
			await page.setViewport( {'width' : wd,
			'height' : ht} );
			if ( counter < 10 ) {
				await page.screenshot({
					path: dir+'event0'+counter+'.png'
				});
			}
			else {
				await page.screenshot({
					path: dir+'event'+counter+'.png'
				});
			}
		}
	}
	await browser.close();
};

function mkdir (dirPath, callback) {
	fs.mkdir(dirPath, (err) => {
		callback(err && err.code !== 'EEXIST' ? err : null);
	});
}

function readFile (file) {
	var rl = readline.createInterface({
		input: fs.createReadStream(file),
		output: process.stdout,
		terminal: false
	});

	rl.on('line', async function (line) {
		// Ignoring the commentsm, if any
		if ( line.slice(0,1) !== "#" ) {
			var args = line.split(",");
			await main(url_head+args[0]+url_tail,args[1],"./"+args[0]+"/");
		}
	});
}

async function main (fbpage,shots,dir) {
	// I need to check later if all the awaits are really useful
	await mkdir(dir, (err) => {
		if (err) { 
			return console.error(err.code);
			process.exit(1);
		}
	});
	await screenshot(fbpage,shots,dir);
}

// Managing all the use cases for passing it arguments to this tool
if ( process.argv.length == 3 && process.argv[2] !== "-f") {
	main(url_head+process.argv[2]+url_tail,undefined,"./"+process.argv[2]+"/");
}
else if ( process.argv.length == 4 && process.argv[2] !== "-f") {
	main(url_head+process.argv[2]+url_tail,process.argv[3],"./"+process.argv[2]+"/");
}
else if (process.argv.length == 4 && process.argv[2] === "-f" ) {
	readFile(process.argv[3]);
}
else usage();
